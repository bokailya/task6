﻿using System;
using System.Linq;
using static System.Drawing.Imaging.PixelFormat;

namespace task6
{
    class Program
    {
        private abstract class Convolution
        {
            public Convolution(System.Drawing.Bitmap bitmap, uint size)
            {
                input = bitmap.Clone() as System.Drawing.Bitmap;
                kernelSize = size;
                kernel = new double[2 * kernelSize + 1, 2 * kernelSize + 1];
            }


            public System.Drawing.Bitmap Apply(bool parallel, bool safe)
            {
                const uint NumberOfComponents = 4;

                System.Drawing.Color resultPixel(
                        System.Drawing.Bitmap input, uint y, uint x)
                {
                    return
                        System.Drawing.Color.FromArgb(
                                (int)
                                BitConverter.ToUInt32(
                                    (
                                     from componentName
                                     in new string[]{"B", "G", "R", "A"}
                                     select
                                     (byte)
                                     Math.Round(
                                         (from j
                                          in
                                          Enumerable.Range(
                                              (int)-kernelSize,
                                              (int)(2 * kernelSize + 1))
                                              from i
                                              in
                                              Enumerable.Range(
                                                  (int)-kernelSize,
                                                  (int)(2 * kernelSize + 1))
                                              select
                                              kernel[
                                              kernelSize + j, kernelSize + i]
                                              *
                                              (byte)
                                              typeof(System.Drawing.Color)
                                              .GetProperty(componentName)
                                              .GetValue(
                                                  input.GetPixel(
                                                      (int)
                                                      ToValidFirstIndex(
                                                          (int)x - i),
                                                      (int)
                                                      ToValidSecondIndex(
                                                          (int)y - j)))
                                              ).Sum())).ToArray()));
                }


                int closestInRange(int start, int end, int value)
                {
                    return
                        value < start ? start : value > end ? end : value;
                }


                uint ToValidFirstIndex(int index)
                {
                    return (uint)closestInRange(0, input.Width - 1, index);
                }


                uint ToValidSecondIndex(int index)
                {
                    return (uint)closestInRange(0, input.Height - 1, index);
                }


                unsafe int resultPixelUnsafe(
                        byte *inputArray, int stride, uint y, uint x)
                {
                    int pixel = 0;
                    byte *writeByte = (byte *)&pixel;
                    foreach (byte pixelByte
                            in
                            from component
                            in Enumerable.Range(0, (int)NumberOfComponents)
                            select
                            (byte)
                            Math.Round(
                                (
                                 from j
                                 in
                                 Enumerable.Range(
                                     (int)-kernelSize,
                                     (int)(2 * kernelSize + 1))
                                     from i
                                     in
                                     Enumerable.Range(
                                         (int)-kernelSize,
                                         (int)(2 * kernelSize + 1))
                                     select
                                     kernel[kernelSize + j, kernelSize + i]
                                     *
                                     inputArray[
                                     ToValidSecondIndex((int)y - j) * stride
                                     + ToValidFirstIndex((int)x - i)
                                     * NumberOfComponents
                                     + component]).Sum()))
                        *writeByte++ = pixelByte;
                    return pixel;
                }


                System.Drawing.Bitmap result
                    = new System.Drawing.Bitmap(
                            input.Width, input.Height, input.PixelFormat);
                if (safe)
                    if (parallel)
                        System.Threading.Tasks.Parallel.For(
                                0,
                                input.Height,
                                y
                                =>
                                {
                                    foreach (
                                            uint x
                                            in Enumerable.Range(0, input.Width))
                                        result.SetPixel(
                                                (int)x,
                                                y,
                                                resultPixel(input, (uint)y, x));
                                });
                    else
                        // Single Thread
                        foreach (uint y in Enumerable.Range(0, input.Height))
                            foreach (uint x in Enumerable.Range(0, input.Width))
                                result.SetPixel(
                                        (int)x,
                                        (int)y,
                                        resultPixel(input, y, x));
                else
                    // Unsafe
                {
                    System.Drawing.Imaging.BitmapData inputData
                        =
                        input.LockBits(
                                new
                                System.Drawing.Rectangle(
                                    0, 0, input.Width, input.Height),
                                System.Drawing.Imaging.ImageLockMode.ReadOnly,
                                Format32bppPArgb);
                    System.Drawing.Imaging.BitmapData resultData
                        =
                        result.LockBits(
                                new
                                System.Drawing.Rectangle(
                                    0, 0, input.Width, input.Height),
                                System.Drawing.Imaging.ImageLockMode.WriteOnly,
                                Format32bppPArgb);
                    unsafe
                    {
                        byte *inputArray = (byte *)inputData.Scan0;
                        int resultStride
                            = resultData.Stride / (int)NumberOfComponents;
                        if (parallel)
                            System.Threading.Tasks.Parallel.For(
                                    0,
                                    input.Height,
                                    y
                                    =>
                                    {
                                        int *writePointer
                                        =
                                        (int *)resultData.Scan0
                                        + resultStride * y;
                                        foreach (
                                                uint x
                                                in
                                                Enumerable.Range(
                                                    0, input.Width))
                                            *writePointer++
                                            =
                                            resultPixelUnsafe(
                                                    inputArray,
                                                    inputData.Stride,
                                                    (uint)y,
                                                    x);
                                    });
                        else
                            // Single Thread
                        {
                            int *writePointer = (int *)resultData.Scan0;
                            foreach (
                                    uint y in Enumerable.Range(0, input.Height))
                            {
                                foreach (
                                        uint x
                                        in Enumerable.Range(0, input.Width))
                                    *writePointer++
                                    =
                                    resultPixelUnsafe(
                                            inputArray, inputData.Stride, y, x);
                                writePointer += resultStride - input.Width;
                            }
                        }
                    }
                    input.UnlockBits(inputData);
                    result.UnlockBits(resultData);
                }
                return result;
            }


            private System.Drawing.Bitmap input;

            protected double[,] kernel;
            protected uint kernelSize;
        }

        private class GaussianBlur : Convolution
        {
            public GaussianBlur(System.Drawing.Bitmap bitmap, uint size)
                : base(bitmap, size)
            {
                double standardNormalDistributionProbabiltyDensityFunction(
                        uint argument)
                {
                    uint Square(uint value)
                    {
                        return value * value;
                    }


                    return
                        Math.Exp(-(double)Square(argument) / 2)
                        / Math.Sqrt(2 * Math.PI);
                }


                double[]
                    standardNormalDistributionProbabiltyDensityFunctionArray
                    = (
                            from i
                            in Enumerable.Range(0, (int)(kernelSize + 1))
                            select
                            standardNormalDistributionProbabiltyDensityFunction(
                                (uint)i)).ToArray();

                foreach (int j in Enumerable.Range(0, (int)(kernelSize + 1)))
                    foreach (
                            int i in Enumerable.Range(0, (int)(kernelSize + 1)))
                        kernel[j, i]
                        =
                        kernel[j, 2 * kernelSize - i]
                        =
                        kernel[2 * kernelSize - j, i]
                        =
                        kernel[2 * kernelSize - j, 2 * kernelSize - i]
                        =
                        standardNormalDistributionProbabiltyDensityFunctionArray
                        [
                        kernelSize - i]
                        *
                        standardNormalDistributionProbabiltyDensityFunctionArray
                        [
                        kernelSize - j];
            }
        }

        private enum ExitCode {OK, Error};

        private static int Main(string[] args)
        {
            const string ProgramName = "task6";

            try
            {
                const uint KernelSize = 16;
                const string
                    InputFile = "input",
                    SingleThreadSafeOutputFile = "output_single_thread_safe",
                    SingleThreadUnsafeOutputFile
                        = "output_single_thread_unsafe",
                    ParallelSafeOutputFile = "output_parallel_safe",
                    ParallelUnsafeOutputFile = "output_parallel_unsafe";

                using(
                        System.Drawing.Bitmap input
                        = new System.Drawing.Bitmap(InputFile))
                {
                    GaussianBlur blurHandle
                        = new GaussianBlur(input, KernelSize);
                    System.Diagnostics.Stopwatch stopwatch
                        = new System.Diagnostics.Stopwatch();

                    long singleThreadTime;
                    stopwatch.Start();
                    using (
                            System.Drawing.Bitmap unsafeResult
                            = blurHandle.Apply(false, false))
                    {
                        stopwatch.Stop();
                        unsafeResult.Save(SingleThreadUnsafeOutputFile);
                    }
                    System.Console.WriteLine(
                            "Single thread Unsafe ticks: "
                            + (singleThreadTime = stopwatch.ElapsedTicks));

                    stopwatch.Restart();
                    using (
                            System.Drawing.Bitmap unsafeResult
                            = blurHandle.Apply(true, false))
                    {
                        stopwatch.Stop();
                        unsafeResult.Save(ParallelUnsafeOutputFile);
                    }
                    System.Console.WriteLine(
                            "Parallel Unsafe ticks:      "
                            + stopwatch.ElapsedTicks
                            + " Speedup: "
                            + (double)singleThreadTime
                            / stopwatch.ElapsedTicks);

                    stopwatch.Restart();
                    using (
                            System.Drawing.Bitmap safeResult
                            = blurHandle.Apply(false, true))
                    {
                        stopwatch.Stop();
                        safeResult.Save(SingleThreadSafeOutputFile);
                    }
                    System.Console.WriteLine(
                            "Single thread Safe ticks:   "
                            + (singleThreadTime = stopwatch.ElapsedTicks));

                    stopwatch.Restart();
                    using (
                            System.Drawing.Bitmap safeResult
                            = blurHandle.Apply(true, true))
                    {
                        stopwatch.Stop();
                        safeResult.Save(ParallelSafeOutputFile);
                    }
                    System.Console.WriteLine(
                            "Parallel safe ticks:        "
                            + stopwatch.ElapsedTicks
                            + " Speedup: "
                            + (double)singleThreadTime
                            / stopwatch.ElapsedTicks);
                }
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(ProgramName + ": " + exception.Message);
                return (int)ExitCode.Error;
            }
            return (int)ExitCode.OK;
        }
    }
}
